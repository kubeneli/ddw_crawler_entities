#!/usr/bin/python

import sys
import re
import urllib2
import urlparse
import threading
import Queue                                                       
from bs import BeautifulSoup                             
#import nltk

import numpy as np

import MySQLdb

crawlQueue = Queue.Queue()                                          # a synchronized queue for multithreaded job
crawlQueue.put("http://daringfireball.net/thetalkshow/")            # putting seed page to the queue

visited = ([])                                                      # an array for already visited pages, it prevents re-visiting pages


pages = (["http://daringfireball.net/thetalkshow/"])                # for indexing pages

row = ([])
col = ([])
data = ([])
outlink = 1

binarySemaphore = threading.Semaphore(1)                            # a semaphore
enough = False

maximum = 1    # maximum of pages that will add new pages into the collection, once reached, only links to already found pages counts

########################################################################


class CrawlerThread(threading.Thread):            # This is the class representing the thread of web crawler
    def __init__(self):

        self.threadId = hash(self)                # each thread gets an id
        threading.Thread.__init__(self)     

    def run(self):

        global enough

        while 1:                                 # endless loop for job
   
            binarySemaphore.acquire()            # finishing sequence for thread
            if len(pages) > maximum:
                 enough = True
            binarySemaphore.release()           # a thread finished its work, the rest of pages in list won't be crawled


            try:
                job = crawlQueue.get(True, 10)          # take an url from the queue, the thread is blocked in case the queue is empty - no active waiting       
            except:
                print ("Thread #%d finished crawling. ") %(self.threadId)
                return

                
            binarySemaphore.acquire()              
            visited.append(job)             # mark the page as visited in order to avoid re-visiting
            print ("Visiting: %s" %job)
                                            # done right after removing item from the queue
            binarySemaphore.release()          
       
            url = urlparse.urlparse(job)    # parse the url

            visitedLink = pages.index(job)  
     
            try:                                 # try to visit the url
                response = urllib2.urlopen(job)


                page = response.read()               # read what I got from visited url
                
                soup = BeautifulSoup(page)           # preparing some soup to make things much easier
            except:                                  # if unreachable - go on
                print ("Couldn't make a soup.")
                continue

            binarySemaphore.acquire()
                         # for testing
            #print "Thread #%d will now get some links. " %(self.threadId)

            binarySemaphore.release()
  
            links = set([]);                    # sweeping the links

            for tag in soup.findAll('a', href=True):    # get all <a href="..."> elements, much better than regex 
                links.add( tag['href'])                 # add all of them to list of links


            for link in links:                  # now make the found links into usable urls

                if link.startswith('/'):        
                    link = 'http://' + url[1] + link
                elif link.startswith('#'):
                    link = 'http://' + url[1] + url[2] + link
                elif not link.startswith('http'):
                    link = 'http://' + url[1] + '/' + link

                if link.startswith('http://daringfireball.net/thetalkshow/'):
                                      
                    binarySemaphore.acquire()
                    if link not in visited:
                        if link not in crawlQueue.queue: # not present in the queue - put it into it 
                           
                            #print "New page: " + link
                            if enough == False:       # if enough flag is not True, new links will be added to queue
                                crawlQueue.put(link) # in case the link is not present in already-visited pages, put it into the queue and start over
                            
                    try:
                        pos = pages.index(link)

                    except ValueError:
                        pos = len(pages)
                        pages.insert( pos, link )
                        
                    row.append( visitedLink )   
                    col.append( pos )           
                    data.append( outlink)
                        
                    
                    binarySemaphore.release()


if __name__ == "__main__":   

   print ("Sup, master? It's nice to see you. Let's crawl some pages tonight.")
   print ("Log: Starting threads.")

   for j in range( 0, 6 ):
        CrawlerThread().start()

   
   while threading.activeCount() > 1:
        continue

   length = len(pages)

   print ("Log: Connecting to database." ) 

   try:
        db = MySQLdb.connect(host="db4free.net", port=3306, user="kubeneli", passwd="mtdF.L4b", db="data27base", charset="utf8", use_unicode=True)
   except:
        print ("Log: Couldn't connect to the database.")
        exit()

   cursor = db.cursor()

   print ("Log: Creating table.")
   try:
        cursor.execute("CREATE TABLE pages2 (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,  url VARCHAR(100) NOT NULL, page_title VARCHAR(100), content TEXT)", ())
   except:
       cursor.execute("DROP TABLE pages2")
       cursor.execute("CREATE TABLE pages2 (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,  url VARCHAR(100) NOT NULL, page_title VARCHAR(100), content TEXT)", ())
  
   cursor.execute("TRUNCATE TABLE pages2")
   print ("Log: Inserting data.")
  
   for i in range( 0, length ):
        content = "..."
        content2 = "..."
        
        try:
            page = urllib2.urlopen(pages[i]).read()
            soup = BeautifulSoup(page)
            title = soup.title.string
            texts = soup.findAll(text=True)

            def visible(element):
                if element.parent.name in ['head', 'title', 'body']:
                    return False
                elif re.match('<!--.*-->', str(element)):
                    return False
                return True

            visible_texts = filter(visible, texts)
            
            content = ''.join(str(elem) for elem in visible_texts)
            content2 = content.replace('\n', ' ').replace('\r', '').replace('  ',' ')

        except: 
            title = "Mysterious page"
            content = ""

        if len(title) == 0:
            title = "Mysterious page"

        if content2 == "":
            continue
        try:
            cursor.execute("INSERT INTO pages2( url, page_title, content ) VALUES (%s, %s, %s)", (pages[i], title, content2))
            
        except:
            continue

   print ("Log: Data inserted.")
   print ("Leaving now, master. Have a nice day :3")
    
  
   


