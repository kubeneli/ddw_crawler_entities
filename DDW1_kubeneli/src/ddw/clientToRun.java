
package ddw;


import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;




public class clientToRun
{
    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    
    java.sql.Connection testConn = null;
     
            
    public void run() throws SQLException
    {
        List <AnnotationSet> myAnnotations = new ArrayList<AnnotationSet> ();
        //ConnectToDatabase();
        try 
        {
            testConn = getConnection();
        }
        catch (SQLException e) 
        {
            System.out.println("nepovedlo se");
            e.printStackTrace();
        }
        if(!isGateInitilised)
            initialiseGate();            // init gate
        try 
        {   
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR"); //doc reset
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");         //tokens
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");   //sentence splitter
            ProcessingResource gazetteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");           // annie gazetteer
            
            File japeOrigFile = new File("C:\\Users\\Elli\\Documents\\gate-embedded-example\\test.jape");       //jape grammar file
            java.net.URI japeURI = japeOrigFile.toURI();
            
            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try 
            {
                transducerFeatureMap.put("grammarURL", japeURI.toURL());        // grammar location
                transducerFeatureMap.put("encoding", "UTF-8");                  // grammar encoding
            }
            catch (MalformedURLException e) 
            {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }
            
            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            // create corpus instance
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            //create pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(gazetteer);
            annotationPipeline.add(japeTransducerPR);
            
            
            // documents
            List<Document> myList = new ArrayList<Document>();
            myList = queryForContent(testConn);
            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("myCorpus");
            for (int i = 0; i < myList.size(); i++)
                corpus.add(myList.get(i));
            

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for(int i=0; i < corpus.size(); i++)
            {
                myAnnotations.clear();
                Document doc = corpus.get(i);
                if (i > 0)
                    System.out.println("----------");
                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();     

                FeatureMap futureMap = null;
                // get all Token annotations
                AnnotationSet personTokens = as_default.get("Person",futureMap);
                AnnotationSet companyTokens = as_default.get("Company",futureMap);
                AnnotationSet cityTokens = as_default.get("City",futureMap);
                AnnotationSet dateTokens = as_default.get("DateAll",futureMap);
                myAnnotations.add(personTokens);
                myAnnotations.add(companyTokens);
                myAnnotations.add(cityTokens);
                myAnnotations.add(dateTokens);
                
                for (int j = 0; j < myAnnotations.size(); j++)
                {
                    printHead(j);
                    ArrayList tokenAnnotations = new ArrayList(myAnnotations.get(j));
                    if (tokenAnnotations.size() == 0) System.out.print("(none);");
                    ArrayList unique = new ArrayList();
                    for (int k = 0; k < tokenAnnotations.size(); k++)
                    {                       
                        Annotation token = (Annotation)tokenAnnotations.get(k);

                        // get the underlying string for the Token
                        Node isaStart = token.getStartNode();
                        Node isaEnd = token.getEndNode();
                        String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                        if (unique.indexOf(underlyingString) == -1)
                        {
                            unique.add(underlyingString);
                            // get a token annotation
                            
                            
                            if (k==0)
                                System.out.print("(" + tokenAnnotations.size() + " total entities); unique entities: " + underlyingString);
                        
                            else
                                System.out.print(", " + underlyingString);

                            // get the features of the token
                            FeatureMap annFM = token.getFeatures();

                            // get the value of the "string" feature
                            //String value = (String)annFM.get((Object)"string");
                            //System.out.println("Token: " + value);
                        }
                    }
                    unique.clear();
                        System.out.println();
                }

            }
        } catch (GateException ex) {
            //Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
      if (testConn != null)
        {
            try 
            {
                closeConnection(testConn);
            }
            catch (SQLException e) 
            {
                System.out.println("Cannost close connection.");
                e.printStackTrace();
            }
        }   
         
        
     }
    
        
    private List<Document> queryForContent(Connection conn) throws SQLException, ResourceInstantiationException
    {
       List<Document> myList = new ArrayList<Document>();
          // our SQL SELECT query. 
      // if you only need a few columns, specify them by name instead of using "*"
      String query = "SELECT * FROM pages2";
 
      // create the java statement
      Statement st = conn.createStatement();
       
      // execute the query, and get a java resultset
      ResultSet rs = st.executeQuery(query);
       
      // iterate through the java resultset
      while (rs.next())
      {
        int id = rs.getInt("id");
        String content = rs.getString("content");
        Document doc = Factory.newDocument(content);
        // print the results
        //System.out.format("%s, %s\n", id, content);
        myList.add(doc);
      }
      return myList;
    }
    
    private void initialiseGate() {
        
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("C:\\_DISK\\programy\\GATE_Developer_8.0");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("C:\\_DISK\\programy\\GATE_Developer_8.0\\plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("C:\\_DISK\\programy\\GATE_Developer_8.0", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            //Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            //Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    public Connection getConnection() throws SQLException 
    {
        Connection test = null;
        String url = "jdbc:mysql://db4free.net:3306/data27base";
        String username = "kubeneli";
        String password = "mtdF.L4b";
        try 
        {
            test = DriverManager.getConnection(url, username, password);
            System.out.println(" Connection established. ");
        } 
        catch (SQLException e) 
        {
            System.out.println(" Error connecting to database:  " + e);
        }
        return test;
    }
     
    public void closeConnection(Connection conn) throws SQLException
    {
         try 
        {
            conn.close();
            System.out.println(" Connection closed. ");
        } 
        catch (SQLException e) 
        {
            System.out.println(" Error with closing connection:  " + e);
        }
         
     }
    
    public void printHead(int j)
    {
        if (j == 0)
            System.out.print("Names ");
        if (j == 1)
            System.out.print("Companies ");
        if (j == 2)
            System.out.print("Cities ");
        if (j == 3)
            System.out.print("Whole Dates ");
    }
     
}
